const { tweet } = require('../models/tweet');
const { getUserTweets } = require('../services/tweets');

async function saveTweets(tweets = []) {
  //TODO Bulk Updates|| Insert Update if exist Insert if doesnot exist

  //Delete   All Old  tweets 

  await tweet.deleteMany({});

  //save the  Latest Tweets 
  let savedTweets = await tweet.insertMany(tweets);
  return savedTweets;
}

async function getTweets() {
  let allTweets = await tweet.aggregate([
    {

      //extract only the fields that client request it
      $project: {
        username: '$user.name',

        //extract just text from tweet and remove any links as https:// 
        text: {
          $substrCP: [
            '$text',
            0,
            {
              $cond: [
                { $lt: [{ $indexOfCP: ['$text', 'https'] }, 0] },
                280,
                { $indexOfCP: ['$text', 'https'] }
              ]
            }
          ]
        },
        retweet_count: 1,
        favorite_count: 1,
        created_at: 1,
        media: '$extended_entities.media'
      }
    }
  ]);

  return allTweets;
}

async function handleTweets() {
  try {

    //go fetch latest tweets  
    let tweets = await getUserTweets();

    //save  tweets 
    await saveTweets(tweets);

    //TODO Real time to push  tweets
  } catch (e) {
    console.log(e);
  }
}

exports.saveTweets = saveTweets;
exports.getTweets = getTweets;
exports.handleTweets = handleTweets;
