const express = require('express');
const router = express.Router();
const { RTMClient } = require('@slack/client');
const { handleTweets } = require('../actions/tweets');
const {slackToken} = require('../config');
 
 const rtm = new RTMClient(slackToken);

 //start Connect With Slack API
rtm.start();

//listen to new messages on slack channel
rtm.on('message', event => {
  if (
    ~event.text
      .toLowerCase()
      .split(' ')
      .indexOf('go')
  )
    handleTweets();
});

module.exports = router;
