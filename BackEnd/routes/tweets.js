const express = require('express');
const router = express.Router();
const { getTweets } = require('../actions/tweets');

router.get('/', async (req, res) => {
  let tweets = await getTweets();
  res.send(tweets);
});

 
module.exports = router;
