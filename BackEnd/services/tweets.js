const Twitter = require('twitter');
const {twitterTokens} = require('../config');

 
const client = new Twitter(twitterTokens);

//send http request to get tweets with screen Name  from twitter API
async function getUserTweets() {
  const params = { screen_name: 'operator_phone' };

  let tweets = await client.get('statuses/user_timeline', params);

  return tweets;
}

exports.getUserTweets = getUserTweets;
