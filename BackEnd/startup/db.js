const mongoose = require('mongoose');

module.exports = function() {
  mongoose
    .connect('mongodb://mongo:27017/zappy', {
      useCreateIndex: true,
      useNewUrlParser: true
    })
    .then(() => console.log('Connetion To MongoDB .......'))
    .catch(err => console.error('error When Connecting to MongoDB...', err));
};
