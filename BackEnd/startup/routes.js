const tweets = require('../routes/tweets');
const slack = require('../routes/slack');

const error = require('../middleware/error');

const cors = require('cors');

const bodyParser = require('body-parser');

module.exports = function(app) {
  app.use(cors());

 
  app.use(bodyParser.json({ limit: '100mb' }));
  app.use(
    bodyParser.urlencoded({
      limit: '100mb',
      extended: true
    })
  );

  app.use('/api/tweets', tweets);
  app.use('/api/slack', slack);


  //using global error handler
  app.use(error);
};
