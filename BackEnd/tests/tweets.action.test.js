const expect = require('chai').expect;

const { saveTweets, getTweets } = require('../actions/tweets');

describe('#saveTweets()', () => {
  context('without arguments', () => {
    it('should return []', async () => {
      expect(await saveTweets([])).to.be.a('array');
    });
  });
});

describe('#getTweets()', () => {
  context('getTweetsthout arguments', () => {
    it('should return []', async () => {
      expect(await getTweets([])).to.be.a('array');
    });
  });
});
