import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule
} from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { DataService } from './services/tweets.service';
import { CrystalGalleryModule } from 'ngx-crystal-gallery';

@NgModule({
  declarations: [AppComponent, TableComponent],
  imports: [BrowserModule, HttpClientModule, CrystalGalleryModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    DataService,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
