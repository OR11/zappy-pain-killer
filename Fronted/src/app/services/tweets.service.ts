import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { RequestOptions, Headers } from '@angular/http';

@Injectable()
export class DataService {
  private headers = new Headers();
  private options: RequestOptions;

  constructor(private http: HttpClient) {
    this.options = new RequestOptions({ headers: this.headers });
  }

  getAccountTweets() {
    return this.http.get(`/api/tweets`).map(res => {
      return res;
    });
  }

  getColumns(): string[] {
    return [ 'text', 'retweet_count', 'favorite_count', 'media'];
  }
}
