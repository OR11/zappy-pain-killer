import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/tweets.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  tweets = [];
  columns: string[];
  myImages = [];

  constructor(private data: DataService) {}

  ngOnInit() {
    this.getAccTweets();
    this.columns = this.data.getColumns();
  }

  getAccTweets() {
    this.data.getAccountTweets().subscribe(
      (data: any) => {
        this.tweets = data;
        console.log(data)
        this.myImages = data.map(tweet =>
          tweet.media
            ? tweet.media.map(md => {
                return {
                  preview: md.media_url_https,
                  full: md.media_url_https,
                  width: 100,
                  height: 100
                };
              })
            : []
        );
      },
      error => {
        console.log(error);
      }
    );
  }
}
